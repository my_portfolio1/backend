from django.contrib.admin import ModelAdmin
from django.contrib import admin

from api.models import Project, Type

class ProjectTypenInline(admin.TabularInline):
    model = Type
    extra = 0

class ProjectAdmin(ModelAdmin):
    inlines = [
        ProjectTypenInline
    ]

    list_display = (
        "id",
        "title",
    )
    search_fields = (
        "title",
    )


class ProjectType(ModelAdmin):
    list_display = (
        "id",
        "name",
    )

admin.site.register(Project, ProjectAdmin)
admin.site.register(Type, ProjectType)