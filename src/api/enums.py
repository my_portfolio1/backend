from django.db import models
from django.utils.translation import gettext_lazy as _

class ProjectType(models.TextChoices):
    frontend = "frontend", _("Frontend")
    backend = "backend", _("Backend")
    devops = "devops", _("Devops")
    design = "design", _("Design")