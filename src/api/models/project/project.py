import os
from datetime import datetime

from stdimage import StdImageField

from api.models.abstract import BaseModel
from django.db import models
from django.utils.translation import gettext_lazy as _

def get_author_avatar_image_path(
    instance, filename: str
) -> str:  # instance: Product
    _, ext = os.path.splitext(filename)
    new_filename = f"{instance.pk}-{datetime.utcnow().strftime('%Y-%m-%d_%H-%M-%S')}"
    if ext:
        new_filename += ext
    return os.path.join(
        f"project/images/{instance.created_at.strftime('%Y/%m/%d')}/",
        new_filename,
    )

class Project(BaseModel):
    title = models.CharField(max_length=256)
    description = models.TextField(max_length=2000)

    avatar = StdImageField(
        upload_to=get_author_avatar_image_path,
        verbose_name=_("Project image"),
        variations={
            "thumbnail": {"width": 750, "height": 350, "crop": True},
            "medium": (750, 350),
        },
        null=True,
        blank=True,
    )

    link_repo = models.CharField(max_length=256, null=True, blank=True)
    link_project = models.CharField(max_length=256, null=True, blank=True)
    technologies = models.TextField(max_length=2000)
    tools = models.TextField(max_length=2000)

    class Meta:
        verbose_name = "Projects"
        verbose_name_plural = "Project"
        ordering = ['-created_at']

    def __str__(self):
        return (
            f"Project #{self.pk} {self.title}"
        )