from django.db import models

from api.enums import ProjectType


class Type(models.Model):
    product = models.ForeignKey(
        "api.Project",
        on_delete=models.PROTECT,
        related_name="types",
    )

    name = models.CharField(
        max_length=32,
        choices=ProjectType.choices,
        default=ProjectType.frontend,
    )

    class Meta:
        verbose_name = "Types"
        verbose_name_plural = "Type"

    def __str__(self):
        return (
            f"Type #{self.pk} {self.name}"
        )
