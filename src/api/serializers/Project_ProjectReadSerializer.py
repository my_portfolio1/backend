from rest_framework import serializers

from api.models import Project, Type


class Type_TypeReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Type
        fields = (
            "id",
            "name"
        )
        read_only_fields = fields


class Project_ProjectListSerializer(serializers.ModelSerializer):
    types = Type_TypeReadSerializer(many=True, read_only=True)

    class Meta:
        model = Project
        fields = (
            "id",
            "title",
            "avatar",
            "types",
            "description",
            "link_repo",
            "link_project",
            "technologies",
            "tools",
        )
        read_only_fields = fields

