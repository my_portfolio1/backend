from rest_framework.routers import DefaultRouter

from api.views.project import Public_ProjectViewSet

router = DefaultRouter()

router.register("project/projects", Public_ProjectViewSet, "project/projects")

urls = [*router.urls]