from rest_framework import mixins, viewsets
from rest_framework.permissions import AllowAny

from api.models import Project
from api.serializers.Project_ProjectReadSerializer import Project_ProjectListSerializer


class Public_ProjectViewSet(
    viewsets.GenericViewSet,
    mixins.RetrieveModelMixin,
    mixins.ListModelMixin,
):
    """This Viewset is needed for any user to work with Influencer shop items"""

    permission_classes = [AllowAny]

    def get_serializer_class(self):
        return Project_ProjectListSerializer

    def get_queryset(self):
        return Project.objects.all()